<?php

namespace DigitalHub\ERPIntegration\Plugin;

use DigitalHub\ERPIntegration\Helper\Config;
use DigitalHub\ERPIntegration\Model\Adapter\OrderDataAdapter;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use DigitalHub\ERPIntegration\Gateway\Services\OrderApi;
use Psr\Log\LoggerInterface;


/**
 * Class OrderManagement
 */
class OrderManagement
{

    /** @var LoggerInterface */
    private $logger;

    /** @var Config */
    protected $configHelper;

    /** @var OrderDataAdapter */
    protected $orderDataAdapter;

    /** @var OrderApi */
    private $orderApi;

    /**
     * OrderManagement constructor.
     * @param Config $configHelper
     * @param OrderDataAdapter $orderDataAdapter
     * @param OrderApi $orderApi
     * @param LoggerInterface $logger
     */
    public function __construct(
        Config $configHelper,
        OrderDataAdapter $orderDataAdapter,
        OrderApi $orderApi,
        LoggerInterface $logger
    ) {
        $this->configHelper = $configHelper;
        $this->orderDataAdapter = $orderDataAdapter;
        $this->orderApi = $orderApi;
        $this->logger = $logger;
    }

    /**
     * Plugin afterPlace
     * @param OrderManagementInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterPlace(OrderManagementInterface $subject, OrderInterface $order)
    {
        if (!$this->configHelper->isEnabled()) {
            return $order;
        }

        try {
            $orderPayload = $this->orderDataAdapter->getData($order);
            $response = $this->orderApi->sendOrder($orderPayload);

            if (!$response || $response->getStatusCode() !== 200) {
                $error = $response ? $response->getBody() : "Unable to get error from response";
                throw new LocalizedException(__($error));
            }
            $this->logger->info(sprintf(__("Order %s was integrated successfully"), $order->getIncrementId()));
        } catch (\Exception $e) {
            $this->logger->error(sprintf(__("Unable to post the Order to the API. Reason: %s"), $e->getMessage()));
        }

        return $order;
    }
}