<?php


namespace DigitalHub\ERPIntegration\Helper;


use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    const BASE_CONFIG_PATH = "erp_integration/general/";
    const MODULE_STATUS = "module_status";
    const API_KEY = "api_key";
    const API_BASE_URL = "api_base_url";
    const API_ORDER_ENDPOINT = "api_order_endpoint";
    const CUSTOMER_CORPORATE_NAME = "customer_corporate_name";
    const CUSTOMER_TRADE_NAME = "customer_trade_name";
    const CUSTOMER_STATE_REGISTRATION = "customer_state_registration";

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get the module status
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->getConfig(self::MODULE_STATUS);
    }

    /**
     * Get the api key
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->getConfig(self::API_KEY);
    }

    /**
     * Get the api base url
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->getConfig(self::API_BASE_URL);
    }

    /**
     * Get the api order endpoint
     * @return mixed
     */
    public function getApiOrderEndpoint()
    {
        return $this->getConfig(self::API_ORDER_ENDPOINT);
    }

    /**
     * Get the customer attribute mapped for corporate name
     * @return mixed
     */
    public function getCorporateNameAttribute()
    {
        return $this->getConfig(self::CUSTOMER_CORPORATE_NAME);
    }

    /**
     * Get the customer attribute mapped for trade name
     * @return mixed
     */
    public function getTradeNameAttribute()
    {
        return $this->getConfig(self::CUSTOMER_TRADE_NAME);
    }

    /**
     * Get the customer attribute mapped for state registration
     * @return mixed
     */
    public function getStateRegistrationAttribute()
    {
        return $this->getConfig(self::CUSTOMER_STATE_REGISTRATION);
    }

    /**
     * Generic config getter
     * @param string $value
     * @return mixed
     */
    private function getConfig($value)
    {
        $config = $this->scopeConfig->getValue(self::BASE_CONFIG_PATH . $value);
        return $config;
    }
}