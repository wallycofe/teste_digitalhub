<?php


namespace DigitalHub\ERPIntegration\Helper;


class Data
{
    /**
     * Return the customer document type
     * @param string $taxVat
     * @return bool|string
     */
    public static function getCustomerDocumentType($taxVat)
    {
        $taxVat = self::removeNonNumericChars($taxVat);
        switch (strlen($taxVat)) {
            case 11:
                return "CPF";
            case 14:
                return "CNPJ";
            default:
                return false;
        }
    }

    /**
     * Remove non numeric chars from the string value
     * @param string $value
     * @return string|null
     */
    public static function removeNonNumericChars($value)
    {
        return preg_replace("/[^0-9.,]/", "", $value);
    }
}