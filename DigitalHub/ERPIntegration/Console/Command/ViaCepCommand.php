<?php


namespace DigitalHub\ERPIntegration\Console\Command;

use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DigitalHub\ERPIntegration\Gateway\Services\ViaCep;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class ViaCepCommand extends Command
{
    /** @var ViaCep */
    protected $viaCepService;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * ViaCepCommand constructor.
     * @param State $state
     * @param ViaCep $viaCepService
     * @param LoggerInterface $logger
     * @param null $name
     */
    public function __construct(
        State $state,
        ViaCep $viaCepService,
        LoggerInterface $logger,
        $name = null
    ) {
        $this->viaCepService = $viaCepService;
        $this->logger = $logger;
        parent::__construct($name);
    }

    /**
     * Configure
     */
    protected function configure()
    {
        $this->setName('viacep:get:ibge');
        $this->setDescription('Executa Consulta na ViaCep');
        $this->addArgument('zipcode', InputArgument::REQUIRED, __('Zip Code'));
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $zipCode = $input->getArgument('zipcode');
        if (!$zipCode) {
            $output->writeln('É necessário informar o zip code');
            die();
        }
        $zipCode = explode('=', $zipCode);
        $zipCode = $zipCode[1];
        try {
            $data = $this->viaCepService->getAddressData($zipCode);
            if ($data->hasData("ibge")) {
                $output->writeln("IBGE_CODE: " . $data->getData("ibge"));
                return;
            }
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $this->logger->error('Erro ao recuperar dados do viacep: ' . $e->getMessage());
        }
    }
}