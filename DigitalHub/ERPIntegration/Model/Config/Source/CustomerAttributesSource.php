<?php

namespace DigitalHub\ERPIntegration\Model\Config\Source;

use Magento\Customer\Ui\Component\Listing\AttributeRepository;

class CustomerAttributesSource implements \Magento\Framework\Option\ArrayInterface
{
    /** @var AttributeRepository */
    protected $attributeRepository;

    /** @var array */
    private $attributes;

    /**
     * CustomerAttributesSource constructor.
     * @param AttributeRepository $attributeRepository
     */
    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
        $this->attributes = $this->attributeRepository->getList();
    }

    /**
     * Options getter
     * @return array
     */
    public function toOptionArray()
    {
        $result = [["value" => false, "label" => __("Not Mapped")]];

        foreach ($this->attributes as $attribute) {
            if (isset($attribute["backend_type"]) && $attribute["backend_type"] == "varchar") {
                array_push(
                    $result, [
                    "value" => $attribute["attribute_code"],
                    "label" => $attribute["frontend_label"] . " ({$attribute["attribute_code"]})"
                ]);
            }
        }
        return $result;
    }

    /**
     * Get options in "key-value" format
     * @return array
     */
    public function toArray()
    {
        $result = ["value" => false, "label" => __("Not Mapped")];

        foreach ($this->attributes as $attribute) {
            if (isset($attribute["backend_type"]) && $attribute["backend_type"] == "varchar") {
                array_push(
                    $result, [
                    $attribute["attribute_code"],
                    $attribute["frontend_label"] . " ({$attribute["attribute_code"]})"
                ]);
            }
        }
        return $result;
    }
}
