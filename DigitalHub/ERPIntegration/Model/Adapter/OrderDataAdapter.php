<?php


namespace DigitalHub\ERPIntegration\Model\Adapter;

use Magento\Framework\DataObject;

class OrderDataAdapter
{

    /** @var OrderCustomerDataAdapter */
    protected $customerDataAdapter;

    /** @var OrderShippingAddressDataAdapter */
    protected $shippingAddressDataAdapter;

    /** @var OrderItemsDataAdapter  */
    protected $itemsDataAdapter;

    /**
     * OrderDataAdapter constructor.
     * @param OrderCustomerDataAdapter $customerDataAdapter
     * @param OrderShippingAddressDataAdapter $shippingAddressDataAdapter
     * @param OrderItemsDataAdapter $itemsDataAdapter
     */
    public function __construct(
        OrderCustomerDataAdapter $customerDataAdapter,
        OrderShippingAddressDataAdapter $shippingAddressDataAdapter,
        OrderItemsDataAdapter $itemsDataAdapter
    ) {
        $this->customerDataAdapter = $customerDataAdapter;
        $this->shippingAddressDataAdapter = $shippingAddressDataAdapter;
        $this->itemsDataAdapter = $itemsDataAdapter;
    }

    /**
     * Get the Order Data to Send Over API
     * @param $order
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData($order)
    {
        $orderData = new DataObject();
        $orderData->addData([
            "customer" => $this->customerDataAdapter->getData($order)->toArray(),
            "shipping_address" => $this->shippingAddressDataAdapter->getData($order->getShippingAddress())->toArray(),
            "items" => $this->itemsDataAdapter->getData($order->getAllVisibleItems()),
            "shipping_method" => $order->getShippingMethod(),
            "payment_method" => $order->getPayment()->getMethod(),
            "payment_installments" => $this->getPaymentInstallments($order->getPayment()),
            "subtotal" => (float)$order->getBaseSubtotal(),
            "shipping_amount" => (float)$order->getBaseShippingAmount(),
            "discount" => (float)$order->getBaseDiscountAmount(),
            "total" => (float)$order->getBaseGrandTotal()
        ]);

        return $orderData->toJson();
    }

    /**
     * Get Order Payment Installments
     * @param $paymentData
     * @return array|mixed
     */
    private function getPaymentInstallments($paymentData)
    {
        $additionalInformation = $paymentData->getAdditionalInformation();
        if (!isset($additionalInformation['installments'])) {
            return [];
        }
        return $additionalInformation['installments'];
    }
}