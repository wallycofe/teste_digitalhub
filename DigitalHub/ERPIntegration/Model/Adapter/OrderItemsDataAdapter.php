<?php


namespace DigitalHub\ERPIntegration\Model\Adapter;

class OrderItemsDataAdapter
{
    /**
     * Get the Order Items
     * @param $items
     * @return array
     */
    public function getData($items)
    {
        $data = [];
        foreach ($items as $item) {
            array_push($data,[
                "sku" => $item->getSku(),
                "name" => $item->getName(),
                "price" => (float) $item->getBaseRowTotalInclTax(),
                "qty" => (float) $item->getQtyOrdered()
            ]);
        }
        return $data;
    }
}