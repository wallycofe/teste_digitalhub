<?php


namespace DigitalHub\ERPIntegration\Model\Adapter;

use Magento\Framework\DataObject;
use Magento\Customer\Api\CustomerRepositoryInterface;
use DigitalHub\ERPIntegration\Helper\Config;
use DigitalHub\ERPIntegration\Helper\Data as DataHelper;
use Magento\Framework\Exception\LocalizedException;

class OrderCustomerDataAdapter
{
    /** @var CustomerRepositoryInterface */
    protected $customerRepository;

    /** @var Config */
    protected $config;

    /**
     * OrderCustomerDataAdapter constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param Config $config
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, Config $config)
    {
        $this->customerRepository = $customerRepository;
        $this->config = $config;
    }

    /**
     * Get the order customer data for the api payload
     * @param $order
     * @return DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData($order)
    {
        $customer = $this->customerRepository->getById($order->getCustomerId());
        $data = new DataObject();
        $data->setData("name", $customer->getFirstname() . " " . $customer->getLastname());
        $data->setData("telephone", $order->getShippingAddress()->getTelephone());

        $customerDocumentType = DataHelper::getCustomerDocumentType($customer->getTaxvat());

        if (!$customerDocumentType) {
            throw new LocalizedException(__("Customer TaxVat is required and valid (CPF or CNPJ)."));
        }

        if ($customerDocumentType == "CPF") {
            $data->setData("cpf_cnpj", $customer->getTaxvat());
        }

        if ($customerDocumentType == "CNPJ") {
            $data->setData("cnpj", $customer->getTaxvat());

            $corporateName = $customer->getFirstname();
            if ($this->config->getCorporateNameAttribute()) {
                $corporateName = $customer->getCustomAttribute($this->config->getCorporateNameAttribute());
            }
            $data->setData("razao_social", $corporateName);

            $tradeName = $customer->getLastname();
            if ($this->config->getTradeNameAttribute()) {
                $tradeName = $customer->getCustomAttribute($this->config->getTradeNameAttribute());
            }
            $data->setData("nome_fantasia", $tradeName);

            if ($this->config->getStateRegistrationAttribute()) {
                $stateRegistration = $customer->getCustomAttribute($this->config->getStateRegistrationAttribute());
                $data->setData("ie", $stateRegistration);
            }
        }

        if ($order->getCustomerDob()) {
            $dob = date("d/m/Y", strtotime($order->getCustomerDob()));
            $data->setData("dob", $dob);
        }

        return $data;
    }
}