<?php


namespace DigitalHub\ERPIntegration\Model\Adapter;

use Magento\Framework\DataObject;
use DigitalHub\ERPIntegration\Gateway\Services\ViaCep;

class OrderShippingAddressDataAdapter
{
    /** @var ViaCep */
    protected $viaCepService;

    /**
     * OrderShippingAddressDataAdapter constructor.
     * @param ViaCep $viaCepService
     */
    public function __construct(ViaCep $viaCepService)
    {
        $this->viaCepService = $viaCepService;
    }

    /**
     * Get the shipping address adapter data
     * @param mixed $address
     * @return DataObject
     */
    public function getData($address)
    {
        $addressExtraData = $this->viaCepService->getAddressData($address->getPostCode());
        $data = new DataObject();

        $data->addData([
            "street" => $address->getStreetLine(1),
            "number" => $address->getStreetLine(2),
            "neighborhood" => $address->getStreetLine(3),
            "additional" => $address->getStreetLine(4),
            "city" => $this->getAddressData("localidade", $address->getCity(), $addressExtraData),
            "city_ibge_code" => $addressExtraData->getData("ibge"),
            "uf" => $this->getAddressData("uf", $address->getRegion(), $addressExtraData),
            "country" => $address->getCountryId()
        ]);
        return $data;
    }

    /**
     * Validate and return address data based on parameters
     * @param $name
     * @param $value
     * @param $extraData
     * @return string
     */
    private function getAddressData($name, $value, $extraData)
    {
        if ($value) {
            return $value;
        }

        if ($extraData->hasData($name)) {
            return $extraData->getData($name);
        }
        return __("Not Available")->getText();
    }
}