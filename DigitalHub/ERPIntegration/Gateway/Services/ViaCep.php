<?php


namespace DigitalHub\ERPIntegration\Gateway\Services;

use DigitalHub\ERPIntegration\Gateway\AbstractRequest;
use Magento\Framework\DataObject;

class ViaCep extends AbstractRequest
{
    const BASE_URL = "https://viacep.com.br/ws/";

    /**
     * Get Address Extra Data from ViaCep
     * @param string $zipCode
     * @return DataObject
     */
    public function getAddressData($zipCode)
    {
        $data = $this->getDataFromApi($this->cleanUpZipCode($zipCode));
        return $data;
    }

    /**
     * Get data from the api
     * @param $zipCode
     * @return DataObject
     */
    private function getDataFromApi($zipCode)
    {
        $uri = self::BASE_URL . "{$zipCode}/json";
        $data = new DataObject();

        $response = $this->get($uri, true);
        if ($response && $response->getStatusCode() == 200) {
            $responseBody = json_decode($response->getBody(), true);
            $data->setData($responseBody);
            return $data;
        }

        $data->setData("ibge", "000000");
        return $data;
    }

    /**
     * Clean up the zip code
     * @param string $zipCode
     * @return mixed
     */
    private function cleanUpZipCode($zipCode)
    {
        return str_replace("-", "", $zipCode);
    }
}