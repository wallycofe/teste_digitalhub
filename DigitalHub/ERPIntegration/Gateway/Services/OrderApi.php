<?php

namespace DigitalHub\ERPIntegration\Gateway\Services;

use DigitalHub\ERPIntegration\Gateway\AbstractRequest;

class OrderApi extends AbstractRequest
{
    /**
     * Send Order Over API
     * @param $payload
     * @return \Zend\Http\Response
     */
    public function sendOrder($payload)
    {
        $apiKey = "Bearer " . $this->config->getApiKey();
        $endpoint = $this->config->getBaseUrl() . $this->config->getApiOrderEndpoint();
        return $this->post($endpoint, $payload, true, $apiKey);
    }
}